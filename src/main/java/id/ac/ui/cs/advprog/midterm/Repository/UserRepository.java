package id.ac.ui.cs.advprog.midterm.Repository;

import id.ac.ui.cs.advprog.midterm.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}

